# Convert markdown to BBCode using multiple existing tools

You like to post on forums, but don't like typing out [BBCode][4] or pressing toolbar buttons on a javascript editor?

## Usage

### Bind this script to a hotkey and use it to tranlate highlighted markdown text to BBCode

For example, in a browser text entry window:

* Type markdown
* Select all (Ctrl-A)
* Press hotkey bound to execute `md2bb-existing.sh`

### On the command line

It takes input from a pipe and prints to STDOUT.

	$ cat README.md | ./md2bb-existing.sh

## Note

Not all BBCOde is implementable in pure, simple markdown. Suitable markdown extensions exist but are hardly less cumbersome to type than BBCode.

Typed BBCode will be left as is.

Some output might not be permissible in some BBCode implementations:

- `[h]` (header) is not known by all
- nested tags are not always possible
- usage of quotes, e.g. for URLs, seems inconsistent

## Dependencies

- `xclip`
- `markdown` (I use the [discount][2] variant)
- `sed`
- `perl` with the `perl-html-parser` class, providing the `HTML::Entities` module

Optional:

- `xdotool` (without it, the script won't automatically paste, just copy to the clipboard)

### PS

This used to be a larger repo containing a failed rewrite of the [original markdown perl script][1].
In its current state it hardly justifies it's own repo anymore, but works much better!

[1]: https://daringfireball.net/projects/downloads/Markdown_1.0.1.zip
[2]: https://www.pell.portland.or.us/~orc/Code/discount/
[3]: https://github.com/tdewolff/minify
[4]: https://www.bbcode.org/
